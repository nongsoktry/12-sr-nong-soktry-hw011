import React from "react";
import { ListGroup, Card } from "react-bootstrap";

function List(props) {
  let paddingLeft = {paddingLeft:"20px"};
  return (
    <React.Fragment>
      <Card>
        <h2 style={paddingLeft}>Result History</h2>
      </Card>
  <ListGroup>{props.item}</ListGroup>
    </React.Fragment>
  );
}
export default List;
